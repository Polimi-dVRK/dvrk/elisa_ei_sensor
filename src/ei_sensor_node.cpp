//
// Created by tibo on 25/05/18.
// Modified by simo on 31/05/18
//


#include <ros/ros.h>

#include <std_msgs/Bool.h>
#include <std_msgs/Float64.h>
#include <std_msgs/String.h>
#include <sensor_msgs/JointState.h>

#include "elisa_ei_sensor/ei_device_driver.hpp"
#include "elisa_ei_sensor/ei_tissue_classifier.hpp"

class EISensor {

public:
    EISensor(ros::NodeHandle& nh, std::unique_ptr<EIDeviceDriver>&device, const std::string& arm_namespace)
        : _nh(nh), _device(std::move(device))
    {
        _has_contact_pub = _nh.advertise<std_msgs::Bool>("has_contact", 1, true);
        _insertion_depth_pub = _nh.advertise<std_msgs::Float64>("insertion_depth", 1, true);
        _tissue_type_pub = _nh.advertise<std_msgs::String>("tissue_type", 1, true);
        
        const std::string joint_state_topic = ros::names::append(arm_namespace, "state_joint_current");
        _arm_joint_state = _nh.subscribe<sensor_msgs::JointState>(
            joint_state_topic, 1, &EISensor::on_joint_state_message, this);
        check_published(_arm_joint_state);
        
        const std::string jaw_state_topic = ros::names::append(arm_namespace, "state_jaw_current");
        _arm_jaw_state = _nh.subscribe<sensor_msgs::JointState>(
            jaw_state_topic, 1, &EISensor::on_jaw_state_message, this);
        check_published(_arm_jaw_state);
    }
    
    void update() {
        _had_contact_before = _has_contact_now;

        std::cerr << "Measuring impedance ...\n";
        try {
            ImpedanceMeasurement impedance = _device->measure_impedance();
        } catch (const parse_error::error& err) {
            ROS_ERROR_STREAM("Failed to parse serial message. " << boost::diagnostic_information(err));
        }
        std::cerr << "Done\n";

    }

    void test_classifier(){
        _had_contact_before = _has_contact_now;
        ImpedanceMeasurement scan_data;
        try {
            for (int i = 0; i < 20; i++) {
                scan_data = _device->measure_impedance();
                _impedance_data.push_back({scan_data.frequency, scan_data.value, 0.08, 0.003});

                std_msgs::Float64 real;
                real.data = std::abs(scan_data.value);
                _insertion_depth_pub.publish(real);
            }

            _tissue_type = _classifier.classify_data(_impedance_data);
        } catch (const parse_error::error& err) {
            ROS_ERROR_STREAM("Failed to parse serial message. " << boost::diagnostic_information(err));
        }


        std_msgs::Bool has_contact_msg;
        _has_contact_now = _classifier.has_contact(scan_data);
        has_contact_msg.data = _has_contact_now;
        _has_contact_pub.publish(has_contact_msg);
        std::cout << "has contact: " << _has_contact_now << std::endl;

        // std_msgs::Float64 current_depth_msg;
        // current_depth_msg.data = _current_depth;
        // _insertion_depth_pub.publish(current_depth_msg);
        // std::cout << "depth: " << _current_depth << std::endl;

        // std_msgs::String tissue_type_msg;
        // tissue_type_msg.data = to_string(_tissue_type);
        // _tissue_type_pub.publish(tissue_type_msg);
        // std::cout << "type: " << to_string(_tissue_type) << std::endl;
    }

    void update_with_one_freq() {
        _had_contact_before = _has_contact_now;
        ImpedanceMeasurement scan_data;
        double current_depth = 0.;
        try {
             scan_data = _device->measure_impedance();

            _has_contact_now = _classifier.has_contact(scan_data);

            if (!_had_contact_before and !_has_contact_now) {
                // No need to classify if we know that we're not in contact with anything
                _tissue_type = TissueType::Unknown;

            } else if (_had_contact_before and !_has_contact_now) {
                // We used to be in contact but now we're not anymore. We've moved out of the tissue
                _tissue_type = TissueType::Unknown;
                _first_contact_insertion = std::numeric_limits<double>::quiet_NaN();

                // Since we've reached the end of the acquisition we can discard the data
                _impedance_data.clear();

            } else if (!_had_contact_before and _has_contact_now) {
                // We weren't in contact before but we are now. We''ve moved into the tissue
                _tissue_type = TissueType::Unknown;
                _first_contact_insertion = _current_insertion;

            } else if (_had_contact_before and _has_contact_now) {
                // We still have contact and can continue the acquisition process

                // The current insertion depth is the amout of z motion since we made contact. If we go
                // too deep the data becomes unreliable and should be discarded.


                current_depth = _first_contact_insertion - _current_insertion;
                // std::cout << "current depth: " << current_depth << "; -max:" << -max_insertion_depth <<
                //            "; cd< -0.002: " << (current_depth < - .002) <<
                //            "; cd > - max: " << (current_depth > - max_insertion_depth) << std::endl;

                if (current_depth < - .002 and current_depth > - max_insertion_depth) {
                    std::cout << "Acquire" << std::endl;
                    _collected_samples ++;
                    scan_data = _device->measure_impedance();
                    _impedance_data.push_back({scan_data.frequency, scan_data.value, _jaw_angle, current_depth});
                    if(_collected_samples >= 20){
                        _tissue_type = _classifier.classify_data(_impedance_data);
                        _collected_samples = 0;
                        _impedance_data.clear();
                    }

                }

            }

            std_msgs::Bool has_contact_msg;
            has_contact_msg.data = _has_contact_now;
            _has_contact_pub.publish(has_contact_msg);

            std_msgs::Float64 current_depth_msg;
            current_depth_msg.data = current_depth;
            _insertion_depth_pub.publish(current_depth_msg);

            std_msgs::String tissue_type_msg;
            tissue_type_msg.data = to_string(_tissue_type);
            _tissue_type_pub.publish(tissue_type_msg);

        } catch (const parse_error::error& err) {
            ROS_ERROR_STREAM("Failed to parse serial message. " << boost::diagnostic_information(err));
        }
    }
    
    double max_insertion_depth = .005; // 5mm;;
    double min_depth_step = .0005; // 0.5mm;
    
private: /* Callbacks */
    
    void on_joint_state_message(const sensor_msgs::JointStateConstPtr& joint_state) {
        // If the robot hasn't been homed we won't see any data in the messages

        if (joint_state->position.empty()) {
            return;
        }
        
        if (joint_state->name[2] != "outer_insertion") {
            ROS_ERROR_THROTTLE(5,
                "Wrong joint name. Expected joint #2 to be \"outer_insertion\" but it was \"%s\"",
                joint_state->name[2].c_str()
            );
            return;
        }

        _current_insertion = joint_state->position[2];

//        if (_has_contact_now) {
//            const auto current_insertion = joint_state->position[2];
//            _current_insertion = current_insertion;
//            std::cout << _current_insertion << std::endl;
//
//        } else {
//            _current_insertion = std::numeric_limits<double>::quiet_NaN();
//        }
    }
    
    void on_jaw_state_message(const sensor_msgs::JointStateConstPtr& jaw_state) {
        // If the robot hasn't been homed we won't see any data in the messages
        if (jaw_state->position.empty())
            return;
    
        if (jaw_state->position.size() != 1) {
            ROS_ERROR_THROTTLE(5,
                "Unexpected message size. The jaw state message should contain only 1 entry but it contained %zu",
                jaw_state->position.size()
            );
            return;
        }

        _jaw_angle = jaw_state->position[0];
        // std::cout << "New jaw angle: " << _jaw_angle;
    }
    
    bool check_published(const ros::Subscriber& sub) {
        if (sub.getNumPublishers() < 1) {
            ROS_WARN_STREAM(
                "" << "Subscribed topic \"" << sub.getTopic() << "\" does not seem to be published ("
                   << sub.getNumPublishers() << " publishers)"
            );
            return false;
        }
        
        return true;
    }
    
private:
    
    /* State Variables */
    double _current_insertion = std::numeric_limits<double>::quiet_NaN();
    double _jaw_angle = std::numeric_limits<double>::quiet_NaN();
    
    bool _has_contact_now = false;
    bool _had_contact_before = false;
    TissueType _tissue_type = TissueType::Unknown;

    int _collected_samples = 0;
    
    /// Insertion depth when we first made contact with the data
    double _first_contact_insertion = 0.;
    
    EITissueClassifier _classifier;
    
    std::vector<EIClassifierSample> _impedance_data;
    
    /* Configuration Variables */
    
    double _min_depth_step = 0.;
    
    double _max_insertion = 0.;
    
    /* ROS Stuff - Subscribers & Publishers */
    
    ros::NodeHandle _nh;
    
    ros::Publisher _has_contact_pub;
    ros::Publisher _insertion_depth_pub;
    ros::Publisher _tissue_type_pub;
    
    ros::Subscriber _arm_joint_state;
    ros::Subscriber _arm_jaw_state;
    
    /* EI Sensor Driver Stuff */
    std::unique_ptr<EIDeviceDriver> _device;
};


int main(int argc, char**argv) {
    ros::init(argc, argv, "elisa_ei_sensor");
    
    ros::NodeHandle nh;
    ros::NodeHandle pnh("~");
    
    double sample_rate = 25.;  // Update Frequency [Hz]
    pnh.getParam("sample_rate", sample_rate);
    
    std::string device_serial_port = "/dev/ACM0";
    pnh.getParam("serial_port", device_serial_port);
    
    int device_baud_rate = 38400;
    pnh.getParam("baud_rate", device_baud_rate);
    
    auto ei_device = std::make_unique<EIDeviceDriver>(device_serial_port, device_baud_rate);
    
    std::string arm_namespace;
    if (!pnh.getParam("arm_namespace", arm_namespace)) {
        ROS_ERROR("Missing required parameter. The \"arm_namespace\" parameter must be specified.");
        return -1;
    }
    
    EISensor ei_sensor(nh, ei_device, arm_namespace);
    pnh.getParam("max_depth", ei_sensor.max_insertion_depth);
    pnh.getParam("min_depth_step", ei_sensor.min_depth_step);
    
    ros::Rate loop_rate(sample_rate);
    while (ros::ok()) {
        ros::spinOnce();
        ei_sensor.update_with_one_freq();
        // ei_sensor.test_classifier();

        loop_rate.sleep();
    }
    
    return 0;
}