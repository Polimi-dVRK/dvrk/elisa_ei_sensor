//
// Created by simo on 25/05/18.
//

#include <iostream>
#include "../../include/elisa_ei_sensor/ei_tissue_classifier.hpp"

const double pi = 3.14159265358979323846;

// below is the gaussian distribution of 3 tissues: liver, muscle and fat
// each column represents the distribution with a different jaw opening distance (one of 2mm,4mm,6mm,8mm,10mm,12mm) and the first row is the mean and the second row is the std
double liver_gaussian[6][2] = { { 405.6, 109.6 }, { 373.5, 88.5 }, { 461.9, 108.8 }, { 326.5, 91.2 }, { 330.3, 78.3 }, { 338.2, 92.8 } };
double muscle_gaussian[6][2] = { { 904.6, 150.5 }, { 720.7, 186.7 }, { 783.3, 206.7 }, { 824.2, 187.2 }, { 784.5, 184.2 }, { 772.4, 154.6 } };
double fat_gaussian[6][2] = { { 33.8, 22.3 }, { 30.9, 19.8 }, { 28.2, 19.8 }, { 52.7, 29.4 }, { 25.0, 11.7 }, { 20.1, 10.5 } };
double lung_gaussian[6][2] = { { 93.6, 17.4 }, { 84.5, 14.3 }, { 86.8, 11.2 }, { 97.4, 37.4 }, { 91.0, 16.3 }, { 100.0, 23.3 } };

bool EITissueClassifier::has_contact(ImpedanceMeasurement sample) {
    bool contact = false;
    if (std::abs(sample.value) > _contact_threshold){
        contact = true;
    }
    return contact;
}

// the function below is how the machine determine the tissue type
TissueType EITissueClassifier::classify_data(std::vector<EIClassifierSample> samples) {
    if (samples.empty())
        return TissueType::Unknown;

    double magnitude_average = 0;
    double jaw_angle_average = 0;
    for (auto sample : samples) {
        magnitude_average += std::abs(sample.value);
        jaw_angle_average += sample.jaw_angle;
    }
    magnitude_average /= samples.size();
    jaw_angle_average /= samples.size();
    double jaw_lenght_average = 40 * sin(jaw_angle_average / 2);
    int converted_jaw_lenght_average = round(jaw_lenght_average / 2 + 0.5) - 1;

    double likelihood[4];
    // Liver -> 1
    likelihood[0] = std::abs((magnitude_average - liver_gaussian[converted_jaw_lenght_average][0]) /
                               liver_gaussian[converted_jaw_lenght_average][1]);
    // Muscle -> 2
    likelihood[1] = std::abs((magnitude_average - muscle_gaussian[converted_jaw_lenght_average][0]) /
                               muscle_gaussian[converted_jaw_lenght_average][1]);
    // Fat -> 3
    likelihood[2] = std::abs((magnitude_average - fat_gaussian[converted_jaw_lenght_average][0]) /
                               fat_gaussian[converted_jaw_lenght_average][1]);
    // Lung -> 4
    likelihood[3] = std::abs((magnitude_average - lung_gaussian[converted_jaw_lenght_average][0]) /
                               lung_gaussian[converted_jaw_lenght_average][1]);

    int counter_result = 0; // 0 -> unknown, 1 -> Liver, 2 -> Muscle, 3 -> Fat, 4 -> Lung.
    for (int i = 1; i < 4; i++) {
        if (likelihood[counter_result] > likelihood[i])
            counter_result = i;
    }

    TissueType resulting_class = TissueType::Unknown;
    if (likelihood[counter_result] <= 3) {
        switch (counter_result) {
            case 0:
                resulting_class = TissueType::Liver;
                break;
            case 1:
                resulting_class = TissueType::Muscle;
                break;
            case 2:
                resulting_class = TissueType::Fat;
                break;
            case 3:
                resulting_class = TissueType::Lung;
                break;
        }
    }
    return resulting_class;
}

