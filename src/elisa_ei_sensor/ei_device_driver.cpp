//
// Created by tibo on 25/05/18.
//

#include <iostream> // For debugging

#include <boost/asio.hpp>
#include <boost/asio/serial_port_base.hpp>
#include <ros/ros.h>

#include "elisa_ei_sensor/ei_device_driver.hpp"

namespace asio = boost::asio;

EIDeviceDriver::EIDeviceDriver()
    // : _io_service(), _serial_port(_io_service)
{
    // Do Nothing
}

EIDeviceDriver::EIDeviceDriver(const std::string serial_port, const int baud_rate)
//     : _io_service(), _serial_port(_io_service, serial_port) {
{
    _serial.setPort(serial_port);
    _serial.setBaudrate(baud_rate);
    _serial.setFlowcontrol(serial::flowcontrol_none);
    _serial.setStopbits(serial::stopbits_one);
    _serial.setParity(serial::parity_none);

    serial::Timeout timeout = serial::Timeout::simpleTimeout(1000);
    _serial.setTimeout(timeout);

    _serial.open();

    if (!_serial.isOpen())
        std::cerr << "serial not open. Oops. \n";



}

ImpedanceMeasurement EIDeviceDriver::measure_impedance() {
    write_message('m'); // m for measurment
    ros::Rate(100).sleep();
    Response response = parse_message( read_line() );
    
    if (response.type != ResponseType::Measure) {
        BOOST_THROW_EXCEPTION(protocol_error::error()
            << protocol_error::description("Expected type to be \"Measure\"")
            << protocol_error::response_body(response.original_message)
        );
    }
   
    return { response.excitation_frequency, response.value };
}


std::vector<ImpedanceMeasurement> EIDeviceDriver::scan_impedances() {
    write_message('f'); //f for frequency scan
    
    std::vector<ImpedanceMeasurement> measurements;
    while (true) {
        Response response = parse_message(read_line());
        
        if (response.type == ResponseType::Terminator)
            break;
        
        if (response.type != ResponseType::Scan) {
            BOOST_THROW_EXCEPTION(protocol_error::error()
                << protocol_error::description("Expected type to be \"Scan\"")
                << protocol_error::response_body(response.original_message)
            );
        }
        
        measurements.emplace_back(response.excitation_frequency, response.value);
    }
    
    return measurements;
}


double EIDeviceDriver::measure_temperature() {
    write_message('t'); //t for getting the temperature
    Response response = parse_message( read_line() );
    
    if (response.type != ResponseType::Temperature) {
        BOOST_THROW_EXCEPTION(protocol_error::error()
            << protocol_error::description("Expected type to be \"Measure\"")
            << protocol_error::response_body(response.original_message)
        );
    }
    
    return response.value.real();
}


std::string EIDeviceDriver::read_line() {
    std::string result;

    // std::cerr << "  Reading serial message: ";

    // If the device bugs out make sure that we don't stay blocked in this infinite loop
    size_t characters_read = 0;
    while((++characters_read) < 256) {
        char c;
//        const auto bytes_read = asio::read(
//                _serial_port, asio::buffer(&c, 1), boost::asio::transfer_at_least(0));

        const auto bytes_read = _serial.read((uint8_t*) &c, 1);

        if (bytes_read == 0) break;

        if (c == '\r')
            continue;
        else if (c == '\n')
            break;
        else
            result += c;
        /*
        if (c == '\t') std::cerr << "\\t-";
        else if (c == '\n') std::cerr << "\\n-";
        else if (c == '\r') std::cerr << "\\r-";
        else std::cerr << c << "-";
        */
    }

    // std::cerr << "\n";
    _serial.flush();
    return result;
}


void EIDeviceDriver::write_message(char command) {
    std::string message{ command };
    // _serial_port.write_some(asio::buffer(message, message.size()));
    _serial.write(message);
    ros::Rate(100).sleep();
}


EIDeviceDriver::Response EIDeviceDriver::parse_message(std::string message) {
    namespace err = parse_error;
    
    Response ei_message;
    ei_message.original_message = message;
    
    if (message.empty()) {
        BOOST_THROW_EXCEPTION(err::error() << err::description("Message should not be empty"));
    }

    try {
        size_t current_parse_idx = 0;
        ei_message.type = parse_measurement_type(message, current_parse_idx);

        if (ei_message.type == ResponseType::Temperature) {
            ei_message.value.real(parse_double(message + "\n", current_parse_idx, '\n'));
        } else if (ei_message.type == ResponseType::Terminator) {
            ei_message.value.real(parse_double(message + "\n", current_parse_idx, '\n'));
        } else {
            ei_message.excitation_frequency = parse_double(message, current_parse_idx, '\t');
            ei_message.value.real(parse_double(message, current_parse_idx, '\t'));
            ei_message.value.imag(parse_double(message, current_parse_idx, '\t'));
            //ei_message.time_stamp = parse_double(message, current_parse_idx, '\n');
        }
    } catch (const parse_error::error& err) {
        std::cout << boost::diagnostic_information(err) << "\n";
    }

    return ei_message;
}


EIDeviceDriver::ResponseType EIDeviceDriver::parse_measurement_type(
    std::string message, size_t& current_parse_idx
) const {
    namespace err = parse_error;
    
    const auto field_end = message.find('\t', current_parse_idx);
    if (field_end == std::string::npos) {
        BOOST_THROW_EXCEPTION(err::error() << err::original_string(message));
    }
    
    const std::string type_str = message.substr(current_parse_idx, (field_end - current_parse_idx));
    if (type_str.length() > 1) {
        BOOST_THROW_EXCEPTION(err::error()
            << err::original_string(message)
            << err::failed_field_start(current_parse_idx)
            << err::failed_field_value(std::string("-") + type_str + "-")
            << err::missing_field("Type field should only be 1 character long")
        );
    }

    ResponseType measurement_type = ResponseType::Unknown;
    if (type_str[0] == '1') {
        measurement_type = ResponseType::Measure;
    } else if (type_str[0] == '2') {
        measurement_type = ResponseType::Stream;
    } else if (type_str[0] == '3') {
        measurement_type = ResponseType::Scan;
    } else if (type_str[0] == '4') {
        measurement_type = ResponseType::Temperature;
    } else if (type_str[0] == '9') {
        measurement_type = ResponseType::Terminator;
    }

    
    current_parse_idx = field_end + 1;
    return measurement_type;
}


double EIDeviceDriver::parse_double(std::string message, size_t &current_parse_idx, char delim) const {
    namespace err = parse_error;
    
    const auto field_end = message.find(delim, current_parse_idx);
    if (field_end == std::string::npos) {
        BOOST_THROW_EXCEPTION(err::error()
              << err::original_string(message)
              << err::failed_field_start(current_parse_idx)
              << err::description("Unable to locate field end delimiter")
        );
    }
    
    const std::string field_str = message.substr(current_parse_idx, (field_end - current_parse_idx));
    
    char *first_error = nullptr;
    double field_value = strtod(field_str.c_str(), &first_error);
    
    // If strtod fails then `end` will be set to the first character that caused an error. Thus, if
    // and only if we have an error `end` will have a non null value.
    if (*first_error != '\0') {
        BOOST_THROW_EXCEPTION(err::error()
            << err::original_string(message)
            << err::failed_field_value(std::string("-") + field_str + "-")
            << err::failed_field_start(current_parse_idx)
            << err::description("Failed to convert field value to double")
        );
    }
    
    current_parse_idx = field_end + 1;
    return field_value;
}
