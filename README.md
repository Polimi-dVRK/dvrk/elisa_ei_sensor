

# ELISA Electrical Bio-Impedance Sensor

The ELISA Electrical Bio-Impedance Sensor is a project developed jointly by [Nearlab](http://nearlab.polimi.it/), [IIT (Istituto Italian di Tecnologia)](https://www.iit.it/) and [Università di Verona](http://www.univr.it) to develop a device capable of classifying tissues by measuring their electrical impedance. 

This repository contains the driver code for the device and a ROS node to operate it. It requires no dependencies other that ROS and should be usable with a a simple `git clone` + `catkin build`. 


