//
// Created by tibo on 25/05/18.
//

#pragma once

#include <complex>

// #include <boost/asio/serial_port.hpp>
#include <boost/exception/all.hpp>

#include <serial/serial.h>

struct _error_base : virtual std::exception, virtual boost::exception {};

namespace parse_error {
    struct error : virtual _error_base {};
    
    typedef boost::error_info<struct tag_description, const std::string> description;
    typedef boost::error_info<struct tag_original_string, const std::string> original_string;
    typedef boost::error_info<struct tag_missing_field, const std::string> missing_field;
    typedef boost::error_info<struct tag_failed_field_start, const size_t> failed_field_start;
    typedef boost::error_info<struct tag_failed_field_value, const std::string> failed_field_value;
}

namespace protocol_error {
    struct error : virtual _error_base {};
    
    typedef boost::error_info<struct tag_description, const std::string> description;
    typedef boost::error_info<struct tag_response_body, const std::string> response_body;
}

struct ImpedanceMeasurement {
    double frequency = std::numeric_limits<double>::quiet_NaN();
    std::complex<double> value;
    
    ImpedanceMeasurement() = default;
    
    ImpedanceMeasurement(double frequency, std::complex<double> value)
        : frequency(frequency), value(value)
    {
        // Do Nothing
    }
};

class EIDeviceDriver {
protected: /* Internal Types */
    enum class ResponseType {
        Unknown,      // Default type a.k.a no information available
        Stream,       // Measure the impedance in a loop as fast as possible until Stop is received
        Stop,         // Stop the stream measurements
        Scan,         // Measure the impedance across a pre-defined range of frequencies
        Terminator,   // Indicates that the scan has finished and all data has been returned
        Measure,      // Measure the impedance at a single frequency
        Temperature   // Get the device temperature
    };

    struct Response {
        std::string original_message;
        
        /// Identifier for the type of message
        ResponseType type = ResponseType::Unknown;
        
        /// Thefrequency at which the measurement was performed
        double excitation_frequency = std::numeric_limits<double>::quiet_NaN();
        
        /// The complex impedance measured
        std::complex<double> value = {
            std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN()
        };
        
        /// The time at which the measurement was performed (which unit ?)
        double time_stamp = std::numeric_limits<double>::quiet_NaN();
    };
    
    struct EICommandMessage {
        ResponseType type;
    };
    
    
public:

    EIDeviceDriver();

    EIDeviceDriver(const std::string serial_port, const int baud_rate);
    
    ImpedanceMeasurement measure_impedance();
    
    std::vector<ImpedanceMeasurement> scan_impedances();
    
    double measure_temperature();

protected:
    std::string read_line();
    
    void write_message(char command);
    
    Response parse_message(std::string message);
    
    ResponseType parse_measurement_type(std::string message, size_t& current_parse_idx) const;
    double parse_double(std::string message, size_t &current_parse_idx, char delim) const;
    
private:
    
//    boost::asio::io_service _io_service;
//    boost::asio::serial_port _serial_port;

    serial::Serial _serial;
};


