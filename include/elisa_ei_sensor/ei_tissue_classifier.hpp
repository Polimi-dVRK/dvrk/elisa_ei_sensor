//
// Created by tibo on 25/05/18.
//


#pragma once

#include "ei_device_driver.hpp"

enum class TissueType {
    Unknown,

    Liver,
    Lung,
    Fat,
    Kidney,
    Muscle,
    
    Cancerous,
    Healthy,
    Phantom2,
};

std::string to_string(const TissueType tissue_type) {
    if (tissue_type == TissueType::Liver)
        return "Liver";
    else if (tissue_type == TissueType::Fat)
        return "Fat";
    else if (tissue_type == TissueType::Lung)
        return "Lung";
    else if (tissue_type == TissueType::Kidney)
        return "Kidney";
    else if (tissue_type == TissueType::Muscle)
        return "Muscle";
    else if (tissue_type == TissueType::Cancerous)
        return "Cancerous";
    else if (tissue_type == TissueType::Healthy)
        return "Healthy";
    else if (tissue_type == TissueType::Phantom2)
        return "Phantom2";
    else
        return "Unknown";
}

struct EIClassifierSample {
    /// The frequency at which the electrical impedance measurement was done
    double frequency = std::numeric_limits<double>::quiet_NaN();
    
    /// The complex value of the impedance
    std::complex<double> value = {
        std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN()
    };
    
    /// The opening angle of the jaw of the gripper.
    double jaw_angle = std::numeric_limits<double>::quiet_NaN();
    
    /// How far the tool has penetrated into the tissue.
    double insertion_depth = std::numeric_limits<double>::quiet_NaN();
};

class EITissueClassifier {
public:
    /// classify the sample into two categories: Air or InContact
    bool has_contact(ImpedanceMeasurement sample);
    
    TissueType classify_data(std::vector<EIClassifierSample> samples);

private:
    double _contact_threshold = 7;
    
};


