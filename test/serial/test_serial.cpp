//
// Created by tibo on 07/09/17.
//

#include <gtest/gtest.h>
#include "elisa_ei_sensor/ei_device_driver.hpp"

class TestableEIDevice: public EIDeviceDriver {
public:
    TestableEIDevice() = default;

    using EIDeviceDriver::Response;
    using EIDeviceDriver::ResponseType;
    using EIDeviceDriver::parse_message;
};


TEST(TestEIDeviceDriver, TestParseScanMessage) {

    TestableEIDevice device;
    const std::string serial_message = "2\t100.0\t200.0\t300.0\n";

    TestableEIDevice::Response expected_response;
    expected_response.type = TestableEIDevice::ResponseType::Stream;
    expected_response.excitation_frequency = 100.0;
    expected_response.value = (200.0 + 300.0i);
    expected_response.time_stamp = 123456;


    TestableEIDevice::Response actual_response = device.parse_message(serial_message);

    EXPECT_EQ(expected_response.type, actual_response.type);
    EXPECT_DOUBLE_EQ(expected_response.excitation_frequency, actual_response.excitation_frequency);
    EXPECT_DOUBLE_EQ(expected_response.value.real(), actual_response.value.real());
    EXPECT_DOUBLE_EQ(expected_response.value.imag(), actual_response.value.imag());
    EXPECT_DOUBLE_EQ(expected_response.time_stamp, actual_response.time_stamp);

}

TEST(TestEIDeviceDriver, TestParseTempMessage) {

    TestableEIDevice device;
    const std::string serial_message = "4\t20.0\n";

    TestableEIDevice::Response expected_response;
    expected_response.type = TestableEIDevice::ResponseType::Temperature;
    expected_response.value = 20.0;

    TestableEIDevice::Response actual_response = device.parse_message(serial_message);

    EXPECT_EQ(expected_response.type, actual_response.type);
    EXPECT_DOUBLE_EQ(expected_response.value.real(), actual_response.value.real());

}

TEST(TestEIDeviceDriver, TestMeasureMessage) {

    TestableEIDevice device;
    // const std::string serial_message = "1\t100\t200.0\t300.0\n";
    const std::string serial_message = "1\t100\t0\t0\n";

    TestableEIDevice::Response expected_response;
    expected_response.type = TestableEIDevice::ResponseType::Measure;
    expected_response.excitation_frequency = 100.0;
    expected_response.value = (0 + 0i);
//    expected_response.time_stamp = 123456;


    TestableEIDevice::Response actual_response = device.parse_message(serial_message);

    EXPECT_EQ(expected_response.type, actual_response.type);
    EXPECT_DOUBLE_EQ(expected_response.excitation_frequency, actual_response.excitation_frequency);
    EXPECT_DOUBLE_EQ(expected_response.value.real(), actual_response.value.real());
    EXPECT_DOUBLE_EQ(expected_response.value.imag(), actual_response.value.imag());
}

TEST(TestEIDeviceDriver, TestFreqMessage) {

    TestableEIDevice device;
    const std::string serial_message = "3\t5.0\t200.0\t300.0\n";

    TestableEIDevice::Response expected_response;
    expected_response.type = TestableEIDevice::ResponseType::Scan;
    expected_response.excitation_frequency = 5.0;
    expected_response.value = (200.0 + 300.0i);

    TestableEIDevice::Response actual_response = device.parse_message(serial_message);

    EXPECT_EQ(expected_response.type, actual_response.type);
    EXPECT_DOUBLE_EQ(expected_response.excitation_frequency, actual_response.excitation_frequency);
    EXPECT_DOUBLE_EQ(expected_response.value.real(), actual_response.value.real());
    EXPECT_DOUBLE_EQ(expected_response.value.imag(), actual_response.value.imag());
}

TEST(TestEIDeviceDriver, TestTerminatorMessage) {

    TestableEIDevice device;
    const std::string serial_message = "9\t9.0\n";

    TestableEIDevice::Response expected_response;
    expected_response.type = TestableEIDevice::ResponseType::Terminator;
    expected_response.value = 9.0;

    TestableEIDevice::Response actual_response = device.parse_message(serial_message);

    EXPECT_EQ(expected_response.type, actual_response.type);
    EXPECT_DOUBLE_EQ(expected_response.value.real(), actual_response.value.real());
}